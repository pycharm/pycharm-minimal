/*
 * Copyright 2000-2016 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.intellij.psi.impl.source.tree;

import com.intellij.lang.ASTNode;
import com.intellij.lang.FileASTNode;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.impl.DebugUtil;
import com.intellij.psi.impl.ElementBase;
import com.intellij.util.CharTable;
import org.jetbrains.annotations.NotNull;

//TODO: rename/regroup?

public class SharedImplUtil {
  private static final Logger LOG = Logger.getInstance("#com.intellij.psi.impl.source.tree.SharedImplUtil");
  private static final boolean CHECK_FOR_READ_ACTION = DebugUtil.DO_EXPENSIVE_CHECKS || ApplicationManager.getApplication().isInternal();

  private SharedImplUtil() {
  }
  
  

  public static FileASTNode findFileElement(@NotNull ASTNode element) {
    if (CHECK_FOR_READ_ACTION && element instanceof ElementBase) {
      ApplicationManager.getApplication().assertReadAccessAllowed();
    }
    ASTNode parent = element.getTreeParent();
    while (parent != null) {
      element = parent;
      parent = parent.getTreeParent();
    }

    if (element instanceof FileASTNode) {
      return (FileASTNode)element;
    }
    return null;
  }

  @NotNull
  public static CharTable findCharTableByTree(ASTNode tree) {
    for (ASTNode o = tree; o != null; o = o.getTreeParent()) {
      CharTable charTable = o.getUserData(CharTable.CHAR_TABLE_KEY);
      if (charTable != null) {
        return charTable;
      }
      if (o instanceof FileASTNode) {
        return ((FileASTNode)o).getCharTable();
      }
    }
    throw new AssertionError("CharTable not found in: " + tree);
  }
}
