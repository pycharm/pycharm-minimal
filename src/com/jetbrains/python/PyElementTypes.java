/*
 * Copyright 2000-2014 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jetbrains.python;

import com.intellij.psi.tree.TokenSet;
import com.jetbrains.python.psi.PyElementType;

public interface PyElementTypes {

  PyElementType FUNCTION_DECLARATION = new PyElementType("FUNCTION_DECLARATION");
  PyElementType CLASS_DECLARATION = new PyElementType("CLASS_DECLARATION");
  PyElementType PARAMETER_LIST = new PyElementType("PARAMETER_LIST");

  PyElementType DECORATOR_LIST = new PyElementType("DECORATOR_LIST");

  TokenSet PARAMETER_LIST_SET = TokenSet.create(PARAMETER_LIST);

  PyElementType NAMED_PARAMETER = new PyElementType("NAMED_PARAMETER");
  PyElementType TUPLE_PARAMETER = new PyElementType("TUPLE_PARAMETER");
  PyElementType SINGLE_STAR_PARAMETER = new PyElementType("SINGLE_STAR_PARAMETER");

  TokenSet PARAMETERS = TokenSet.create(NAMED_PARAMETER, TUPLE_PARAMETER, SINGLE_STAR_PARAMETER);

  PyElementType DECORATOR_CALL = new PyElementType("DECORATOR_CALL");

  TokenSet FORMAL_PARAMETER_SET = TokenSet.create(NAMED_PARAMETER);

  PyElementType ARGUMENT_LIST = new PyElementType("ARGUMENT_LIST");

  PyElementType IMPORT_ELEMENT = new PyElementType("IMPORT_ELEMENT");

  PyElementType ANNOTATION = new PyElementType("ANNOTATION");

  PyElementType STAR_IMPORT_ELEMENT = new PyElementType("STAR_IMPORT_ELEMENT");
  PyElementType EXCEPT_PART = new PyElementType("EXCEPT_PART");
  PyElementType PRINT_TARGET = new PyElementType("PRINT_TARGET");
  PyElementType DECORATOR = new PyElementType("DECORATOR");

  // Statements
  PyElementType EXPRESSION_STATEMENT = new PyElementType("EXPRESSION_STATEMENT");
  PyElementType ASSIGNMENT_STATEMENT = new PyElementType("ASSIGNMENT_STATEMENT");
  PyElementType AUG_ASSIGNMENT_STATEMENT = new PyElementType("AUG_ASSIGNMENT_STATEMENT");
  PyElementType ASSERT_STATEMENT = new PyElementType("ASSERT_STATEMENT");
  PyElementType BREAK_STATEMENT = new PyElementType("BREAK_STATEMENT");
  PyElementType CONTINUE_STATEMENT = new PyElementType("CONTINUE_STATEMENT");
  PyElementType DEL_STATEMENT = new PyElementType("DEL_STATEMENT");
  PyElementType EXEC_STATEMENT = new PyElementType("EXEC_STATEMENT");
  PyElementType FOR_STATEMENT = new PyElementType("FOR_STATEMENT");

  PyElementType FROM_IMPORT_STATEMENT = new PyElementType("FROM_IMPORT_STATEMENT");
  PyElementType IMPORT_STATEMENT = new PyElementType("IMPORT_STATEMENT");

  PyElementType GLOBAL_STATEMENT = new PyElementType("GLOBAL_STATEMENT");
  PyElementType IF_STATEMENT = new PyElementType("IF_STATEMENT");
  PyElementType PASS_STATEMENT = new PyElementType("PASS_STATEMENT");
  PyElementType PRINT_STATEMENT = new PyElementType("PRINT_STATEMENT");
  PyElementType RAISE_STATEMENT = new PyElementType("RAISE_STATEMENT");
  PyElementType RETURN_STATEMENT = new PyElementType("RETURN_STATEMENT");
  PyElementType TRY_EXCEPT_STATEMENT = new PyElementType("TRY_EXCEPT_STATEMENT");
  PyElementType WITH_STATEMENT = new PyElementType("WITH_STATEMENT");
  PyElementType WHILE_STATEMENT = new PyElementType("WHILE_STATEMENT");
  PyElementType STATEMENT_LIST = new PyElementType("STATEMENT_LIST");

  PyElementType NONLOCAL_STATEMENT = new PyElementType("NONLOCAL_STATEMENT");

  PyElementType WITH_ITEM = new PyElementType("WITH_ITEM");

  TokenSet LOOPS = TokenSet.create(WHILE_STATEMENT, FOR_STATEMENT);

  // Expressions
  PyElementType EMPTY_EXPRESSION = new PyElementType("EMPTY_EXPRESSION");
  PyElementType REFERENCE_EXPRESSION = new PyElementType("REFERENCE_EXPRESSION");

  PyElementType TARGET_EXPRESSION = new PyElementType("TARGET_EXPRESSION");
  PyElementType INTEGER_LITERAL_EXPRESSION = new PyElementType("INTEGER_LITERAL_EXPRESSION");
  PyElementType FLOAT_LITERAL_EXPRESSION = new PyElementType("FLOAT_LITERAL_EXPRESSION");
  PyElementType IMAGINARY_LITERAL_EXPRESSION = new PyElementType("IMAGINARY_LITERAL_EXPRESSION");
  PyElementType STRING_LITERAL_EXPRESSION = new PyElementType("STRING_LITERAL_EXPRESSION");
  PyElementType NONE_LITERAL_EXPRESSION = new PyElementType("NONE_LITERAL_EXPRESSION");
  PyElementType BOOL_LITERAL_EXPRESSION = new PyElementType("BOOL_LITERAL_EXPRESSION");
  PyElementType PARENTHESIZED_EXPRESSION = new PyElementType("PARENTHESIZED_EXPRESSION");
  PyElementType SUBSCRIPTION_EXPRESSION = new PyElementType("SUBSCRIPTION_EXPRESSION");
  PyElementType SLICE_EXPRESSION = new PyElementType("SLICE_EXPRESSION");
  PyElementType SLICE_ITEM = new PyElementType("SLICE_ITEM");
  PyElementType BINARY_EXPRESSION = new PyElementType("BINARY_EXPRESSION");
  PyElementType PREFIX_EXPRESSION = new PyElementType("PREFIX_EXPRESSION");
  PyElementType CALL_EXPRESSION = new PyElementType("CALL_EXPRESSION");
  PyElementType LIST_LITERAL_EXPRESSION = new PyElementType("LIST_LITERAL_EXPRESSION");
  PyElementType TUPLE_EXPRESSION = new PyElementType("TUPLE_EXPRESSION");
  PyElementType KEYWORD_ARGUMENT_EXPRESSION = new PyElementType("KEYWORD_ARGUMENT_EXPRESSION");
  PyElementType STAR_ARGUMENT_EXPRESSION = new PyElementType("STAR_ARGUMENT_EXPRESSION");
  PyElementType LAMBDA_EXPRESSION = new PyElementType("LAMBDA_EXPRESSION");
  PyElementType LIST_COMP_EXPRESSION = new PyElementType("LIST_COMP_EXPRESSION");
  PyElementType DICT_LITERAL_EXPRESSION = new PyElementType("DICT_LITERAL_EXPRESSION");
  PyElementType KEY_VALUE_EXPRESSION = new PyElementType("KEY_VALUE_EXPRESSION");
  PyElementType REPR_EXPRESSION = new PyElementType("REPR_EXPRESSION");
  PyElementType GENERATOR_EXPRESSION = new PyElementType("GENERATOR_EXPRESSION");
  PyElementType CONDITIONAL_EXPRESSION = new PyElementType("CONDITIONAL_EXPRESSION");
  PyElementType YIELD_EXPRESSION = new PyElementType("YIELD_EXPRESSION");
  PyElementType STAR_EXPRESSION = new PyElementType("STAR_EXPRESSION");
  PyElementType DOUBLE_STAR_EXPRESSION = new PyElementType("DOUBLE_STAR_EXPRESSION");

  PyElementType SET_LITERAL_EXPRESSION = new PyElementType("SET_LITERAL_EXPRESSION");
  PyElementType SET_COMP_EXPRESSION = new PyElementType("SET_COMP_EXPRESSION");
  PyElementType DICT_COMP_EXPRESSION = new PyElementType("DICT_COMP_EXPRESSION");

  TokenSet LIST_LIKE_EXPRESSIONS = TokenSet.create(LIST_LITERAL_EXPRESSION, LIST_COMP_EXPRESSION, TUPLE_EXPRESSION);

  TokenSet STATEMENT_LISTS = TokenSet.create(STATEMENT_LIST);

  TokenSet BINARY_OPS = TokenSet.create(PyTokenTypes.OR_KEYWORD, PyTokenTypes.AND_KEYWORD, PyTokenTypes.LT, PyTokenTypes.GT,
                                        PyTokenTypes.OR, PyTokenTypes.XOR, PyTokenTypes.AND, PyTokenTypes.LTLT, PyTokenTypes.GTGT,
                                        PyTokenTypes.EQEQ, PyTokenTypes.GE, PyTokenTypes.LE, PyTokenTypes.NE, PyTokenTypes.NE_OLD,
                                        PyTokenTypes.IN_KEYWORD, PyTokenTypes.IS_KEYWORD, PyTokenTypes.NOT_KEYWORD, PyTokenTypes.PLUS,
                                        PyTokenTypes.MINUS, PyTokenTypes.MULT, PyTokenTypes.AT, PyTokenTypes.FLOORDIV, PyTokenTypes.DIV,
                                        PyTokenTypes.PERC, PyTokenTypes.EXP);

  TokenSet UNARY_OPS = TokenSet.create(PyTokenTypes.NOT_KEYWORD, PyTokenTypes.PLUS, PyTokenTypes.MINUS, PyTokenTypes.TILDE,
                                       PyTokenTypes.AWAIT_KEYWORD);

  // Parts
  PyElementType IF_PART_IF = new PyElementType("IF_IF");
  PyElementType IF_PART_ELIF = new PyElementType("IF_ELIF");

  PyElementType FOR_PART = new PyElementType("FOR_PART");
  PyElementType WHILE_PART = new PyElementType("WHILE_PART");

  PyElementType TRY_PART = new PyElementType("TRY_PART");
  PyElementType FINALLY_PART = new PyElementType("FINALLY_PART");

  PyElementType ELSE_PART = new PyElementType("ELSE_PART");

  TokenSet PARTS = TokenSet.create(IF_PART_IF, IF_PART_ELIF, FOR_PART, WHILE_PART, TRY_PART, FINALLY_PART, ELSE_PART, EXCEPT_PART);
  TokenSet ELIFS = TokenSet.create(IF_PART_ELIF);
  TokenSet STAR_PARAMETERS = TokenSet.create(NAMED_PARAMETER, STAR_ARGUMENT_EXPRESSION, STAR_EXPRESSION, DOUBLE_STAR_EXPRESSION);
  TokenSet CLASS_OR_FUNCTION = TokenSet.create(CLASS_DECLARATION, FUNCTION_DECLARATION);
  TokenSet IMPORT_STATEMENTS = TokenSet.create(IMPORT_STATEMENT, FROM_IMPORT_STATEMENT);
}
