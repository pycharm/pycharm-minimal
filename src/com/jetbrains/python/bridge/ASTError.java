package com.jetbrains.python.bridge;

import com.intellij.openapi.util.TextRange;

/**
 * @author traff
 */
public class ASTError {
  private final String myMessage;
  private final  TextRange myTextRange;

  public ASTError(String message, TextRange textRange) {
    this.myMessage = message;
    this.myTextRange = textRange;
  }

  public String getMessage() {
    return myMessage;
  }

  public TextRange getTextRange() {
    return myTextRange;
  }
}
