package com.jetbrains.python.bridge;

import com.google.common.collect.Lists;
import com.intellij.lang.ASTNode;
import com.intellij.lang.impl.PsiBuilderImpl;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiErrorElement;
import com.intellij.psi.impl.source.CharTableImpl;
import com.intellij.psi.tree.TokenSet;
import com.jetbrains.python.PyTokenTypes;
import com.jetbrains.python.lexer.PythonIndentingLexer;
import com.jetbrains.python.parsing.PyParser;
import com.jetbrains.python.psi.PyFileElementType;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author traff
 */
public class PythonParser {
  private final ASTNode myAst;

  public PythonParser(String code) {
    PyParser parser = new PyParser();

    myAst = parser.parse(PyFileElementType.INSTANCE, new PsiBuilderImpl(new Project() {
      @NotNull
      @Override
      public String getName() {
        return "MyProject";
      }
    }, TokenSet.create(PyTokenTypes.LINE_BREAK, PyTokenTypes.SPACE, PyTokenTypes.TAB_TOKEN, PyTokenTypes.FORMFEED), TokenSet.create(PyTokenTypes.END_OF_LINE_COMMENT), new PythonIndentingLexer(), new CharTableImpl(), code, null, null, null));
  }

  public List<ASTError> findErrors() {
    ArrayList<ASTError> errors = Lists.newArrayList();
    doFindErrors(myAst, errors, 0);
    return errors;
  }

  private void doFindErrors(ASTNode node, ArrayList<ASTError> errors, int offset) {
    if (node instanceof PsiErrorElement) {
      errors.add(new ASTError(((PsiErrorElement) node).getErrorDescription(), TextRange.from(offset, node.getTextLength())));
    }

    ASTNode child = node.getFirstChildNode();
    while (child != null) {
      doFindErrors(child, errors, offset);

      offset += child.getTextLength();
      child = child.getTreeNext();
    }
  }

}
