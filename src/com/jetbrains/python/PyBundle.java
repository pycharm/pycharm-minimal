package com.jetbrains.python;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

/**
 * @author traff
 */
public class PyBundle {
  private static final Properties PROPERTIES_BUNDLE = loadBundle();

  public static String message(String s) {
    String v = PROPERTIES_BUNDLE.getProperty(s);
    if (v != null) {
      System.out.println(v);
      return v;
    }
    return s;
  }

  @NotNull
  private static Properties loadBundle() {
    Properties p = new Properties();
    try {
      p.load(new StringReader(BUNDLE));
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    return p;
  }

  private final static String BUNDLE = "### parsing\n" +
          "PARSE.expected.expression=expression expected\n" +
          "PARSE.expected.rbracket=']' expected\n" +
          "PARSE.expected.expr.or.comma.or.bracket=expected expression, ',' or ']'\n" +
          "PARSE.expected.in='in' expected\n" +
          "PARSE.expected.for.or.bracket=']' or 'for' expected\n" +
          "PARSE.expected.comma=',' expected\n" +
          "PARSE.expected.colon=':' expected\n" +
          "PARSE.expected.rpar=')' expected\n" +
          "PARSE.expected.lpar='(' expected\n" +
          "PARSE.expected.rbrace='}' expected\n" +
          "PARSE.expected.tick='`' (backtick) expected\n" +
          "PARSE.expected.name=name expected\n" +
          "PARSE.expected.colon.or.rbracket=':' or ']' expected\n" +
          "PARSE.expected.comma.or.rpar=',' or ')' expected\n" +
          "PARSE.expected.else='else' expected\n" +
          "\n" +
          "PARSE.expected.identifier=Identifier expected\n" +
          "PARSE.expected.comma.lpar.rpar=',' or '(' or ')' expected\n" +
          "PARSE.expected.statement.break=Statement break expected\n" +
          "PARSE.expected.@.or.def='@' or 'def' expected\n" +
          "PARSE.expected.formal.param.name=formal parameter name expected";
}