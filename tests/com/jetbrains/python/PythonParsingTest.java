package com.jetbrains.python;

import com.intellij.lang.ASTNode;
import com.intellij.lang.impl.PsiBuilderImpl;
import com.intellij.openapi.project.Project;
import com.intellij.psi.impl.source.CharTableImpl;
import com.intellij.psi.tree.TokenSet;
import com.jetbrains.python.bridge.PythonParser;
import com.jetbrains.python.lexer.PythonIndentingLexer;
import com.jetbrains.python.parsing.PyParser;
import com.jetbrains.python.psi.PyFileElementType;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

/**
 * @author traff
 */
public class PythonParsingTest extends TestCase {
  public void testColonExpected() {
    assertEquals(1, new PythonParser("def foo()\n   pass").findErrors().size());
  }
}
