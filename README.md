# PyCharm Minimal #

A minimal subset of PyCharm codebase to provide lexing, parsing and code insight for Python code.
 

### What is this repository for? ###

* Intended to be translated to Objective-C to use on iOS. 

### To generate Objective-C sources ###

* Install j2objc
* Find j2objc.sh in the project root and change J2OBJC variable to your j2objc installation path
* Launch j2objc.sh, it will generate *.m and *.h files in pycharm-minimal-objc folder